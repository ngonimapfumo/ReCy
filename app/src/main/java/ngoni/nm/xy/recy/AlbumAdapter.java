package ngoni.nm.xy.recy;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.MyViewHolder> {

    private Context mContext;
    private List<Album> albumList;

    public AlbumAdapter(Context context, List<Album> albumList){
        this.mContext = context;
        this.albumList = albumList;
    }

    @Override
    public AlbumAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_card, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AlbumAdapter.MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView title, count;
        private ImageView thumbnail, overflow;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            count= itemView.findViewById(R.id.count);
            thumbnail=itemView.findViewById(R.id.thumbnail_id);
            overflow=itemView.findViewById(R.id.overflow);

        }



    }
}
